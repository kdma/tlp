import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.JSONValue;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;

/**
 * Created with IntelliJ IDEA.
 * User: kdma
 * Date: 19/07/13
 * Time: 17.43
 * To change this template use File | Settings | File Templates.
 */
public class Timeline {
    private JSONArray _timeline;
    private TwitterApiConnector _connector;
    private Long _timestampLimit;
    private String _username;
    private TwitterJsonParser _parser;
    private Collection<Tweet> _postList;


    public Timeline(String username, Long timestampLimit){
        _timeline = new JSONArray();
        _connector = new TwitterApiConnector();
        _timestampLimit = timestampLimit;
        _username = username;
        _parser = new TwitterJsonParser(_timestampLimit);
        _postList = new ArrayList<Tweet>();
    }

    public Collection<Tweet> BuildTimeline() throws IOException {
        JSONArray parsedPosts = GetTweetsUntilDate();
        for (int i = 0, size = parsedPosts.size(); i < size; i++) {
            JSONObject parsedTweet = (JSONObject) parsedPosts.get(i);
            Tweet tweet = new Tweet.Builder(parsedTweet)
                    .Id()
                    .CreationDate()
                    .Timestamp()
                    .FavoriteCount()
                    .Hashtags()
                    .RetweetCount()
                    .Source()
                    .Text()
                    .Urls()
                    .User()
                    .UserMentions()
                    .Build();
            _postList.add(tweet);
        }
        TwitterSearch retriever = new TwitterSearch(_timestampLimit, _postList);
        retriever.FillTimelineWithReplies(_timestampLimit, _username);
        return _postList;
    }


    private JSONArray GetTweetsUntilDate() throws IOException {
        _timeline = RetrieveTimeline();
        return _timeline;
    }

    private JSONArray RetrieveTimeline() throws IOException{
        JSONArray result = GetRawFeed(_username);
        if (_parser.IsLastStatusNewerThanTimestamp(result)) {
            String lastId = GetIdOfLastUnparsedTweet(result);
            return GetNextPageUntilTimestamp(lastId, result);
        } else {
            return _parser.TrimFeedUntilTimestamp(result);
        }
    }

    private JSONArray GetRawFeed(String username) throws IOException {
        String feed = _connector.GetUserTimeline(username);
        return (JSONArray) JSONValue.parse(feed);
    }

    private JSONArray GetNextPageUntilTimestamp(String maxId, JSONArray result) throws IOException {
        String rawFeed = _connector.GetUserTimelineAfterTweetId(_username, maxId);
        JSONArray parsedPosts = (JSONArray) JSONValue.parse(rawFeed);
        if (_parser.IsLastStatusNewerThanTimestamp(parsedPosts)) {
            maxId = GetIdOfLastUnparsedTweet(parsedPosts);
            result = JsonHelper.Concatenate(result, parsedPosts);
            return GetNextPageUntilTimestamp(maxId, result);
        } else {
            result = JsonHelper.Concatenate(result, _parser.TrimFeedUntilTimestamp(parsedPosts));
        }
        _connector.CloseConnection();
        return result;
    }

    private String GetIdOfLastUnparsedTweet(JSONArray parsedTweets) {
        int last = parsedTweets.size() - 1;
        JSONObject tweet = (JSONObject) parsedTweets.get(last);
        return (String) tweet.get("id_str");
    }

}
