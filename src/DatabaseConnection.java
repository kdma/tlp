import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.sql.Statement;

/**
 * Created with IntelliJ IDEA.
 * User: kdma
 * Date: 12/08/13
 * Time: 18.30
 * To change this template use File | Settings | File Templates.
 */
public class DatabaseConnection {

    private final static String USERNAME = "tlpuser";
    private final static String PASSWORD = "tlpuser";
    private final static String PORT = "8888";
    private final static String DB_NAME = "tlp";

    public static Connection GetConnection() {
        try {
            Class.forName("com.mysql.jdbc.Driver");
        } catch (ClassNotFoundException e) {
            System.out.println("No mysql driver Found");
            e.getStackTrace();
        }
        try {
            Connection conn = DriverManager.getConnection("jdbc:mysql://localhost:"+ PORT + "/" + DB_NAME, USERNAME, PASSWORD);
            CreateTablesIfNotExists(conn);
            return conn;
        } catch (SQLException ex) {
            System.out.println("No database named tlp found please create one or wrong access credential");
            ex.getStackTrace();
        }
        return null;
    }

    private static void CreateTablesIfNotExists(Connection conn) throws SQLException{
        Statement createSource = conn.createStatement();
        createSource.executeUpdate(DatabaseHelper.QUERY.CREATE_SOURCE_TABLE);
        createSource.close();
        Statement createStatus = conn.createStatement();
        createStatus.executeUpdate(DatabaseHelper.QUERY.CREATE_STATUS_TABLE);
        createStatus.close();
        Statement insertExample = conn.createStatement();
        insertExample.executeUpdate(DatabaseHelper.QUERY.CREATE_EXAMPLE_ROWS);
        insertExample.close();

    }

}
