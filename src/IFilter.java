import java.util.Collection;

/**
 * Created with IntelliJ IDEA.
 * User: kdma
 * Date: 11/06/13
 * Time: 14.40
 * To change this template use File | Settings | File Templates.
 */
public interface IFilter<E> {
     Collection<E> Filter(Collection<E> input);
}