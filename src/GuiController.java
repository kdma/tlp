import javax.swing.*;
import java.util.Calendar;
import java.util.TimeZone;
import java.util.Timer;
import java.util.TimerTask;

/**
 * Created with IntelliJ IDEA.
 * User: kdma
 * Date: 28/08/13
 * Time: 0.46
 * To change this template use File | Settings | File Templates.
 */
public class GuiController {

    private Timer _timer;
    private StatusFetcher _task;

    public void runDataAggregator(int month, int day, int interval, JLabel updatedAt){
        Long intervalMillisecs = interval * 60 * 1000L;
        _timer = new Timer("Data Aggregator");
        Calendar italyCalendar = Calendar.getInstance(TimeZone.getTimeZone("Europe/Rome"));
        italyCalendar.set(italyCalendar.get(Calendar.YEAR), month, day);
        _task = new StatusFetcher(italyCalendar, interval, updatedAt);
        _timer.schedule(_task, italyCalendar.getTime(), intervalMillisecs);
    }

    public void Stop(){
        _task.cancel();
        _timer.cancel();
        _task.CancelInterval();
        System.out.println(" --------------------------------------------------------------------------------------------");
        System.out.println(" |               Application will stop after finishing last active retrieval                 |");
        System.out.println(" --------------------------------------------------------------------------------------------");
    }
}

class StatusFetcher extends TimerTask {
    private Calendar italyCalendar;
    private int _hours;
    private int _minutes;
    private final static int SECONDS = 0;
    private int _interval;
    private JLabel _updatedAt;

    StatusFetcher(Calendar calendar, int interval, JLabel updatedAt) {
        italyCalendar = calendar;
        _interval = interval;
        _updatedAt = updatedAt;
    }

    public void run() {
        Calendar now = Calendar.getInstance(TimeZone.getTimeZone("Europe/Rome"));
        italyCalendar.set(italyCalendar.get(Calendar.YEAR), italyCalendar.get(Calendar.MONTH), italyCalendar.get(Calendar.DAY_OF_MONTH), _hours, _minutes, SECONDS);
        System.out.println("Fetching statuses from " + italyCalendar.getTime() + " up to " + now.getTime());
        _updatedAt.setText(now.getTime().toLocaleString());
        DataAggregator aggregator = new DataAggregator(italyCalendar);
        try {
            aggregator.InsertResults();
            aggregator.CloseResources();
        } catch (Exception e) {
            e.getStackTrace();
        }
        PrintMessages(now, aggregator);
    }

    private void UpdateTime(Calendar now) {
        _hours = now.get(Calendar.HOUR_OF_DAY);
        _minutes = now.get(Calendar.MINUTE);
    }

    private void PrintMessages(Calendar latestUpdate, DataAggregator aggregator){
        Calendar finishedTime =  Calendar.getInstance(TimeZone.getTimeZone("Europe/Rome"));
        System.out.println("Finished at " + finishedTime.getTime());
        if(_interval > 0){
            System.out.println("Now waiting " + _interval + " minutes " );
        }else{
            System.out.println("Timer stopped resume when you want" );
        }
        System.out.println(aggregator.GetNumberOfRowsInserted() + " statuses inserted into Database");
        aggregator.ResetRowCount();
        UpdateTime(latestUpdate);
    }

    public  void CancelInterval(){
        _interval = 0;
    }
}

