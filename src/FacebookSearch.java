import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.JSONValue;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;

/**
 * Created with IntelliJ IDEA.
 * User: kdma
 * Date: 25/07/13
 * Time: 16.58
 * To change this template use File | Settings | File Templates.
 */
public class FacebookSearch {
    /**
     * Created with IntelliJ IDEA.
     * User: kdma
     * Date: 19/07/13
     * Time: 18.13
     * To change this template use File | Settings | File Templates.
     */

    private FacebookApiConnector _connector;
    private JSONArray _searchResult;
    private FacebookJsonParser _parser;
    private Collection<FacebookPost> _postList;

    public FacebookSearch(Long timestampLimit) {
        _connector = new FacebookApiConnector();
        _searchResult = new JSONArray();
        _parser = new FacebookJsonParser(timestampLimit);
        _postList = new ArrayList<FacebookPost>();
    }

    public Collection<FacebookPost> ExecuteSearch(String hashtag) throws IOException {
        JSONArray parsedPosts = RetrieveResults(hashtag);
        for (int i = 0, size = parsedPosts.size(); i < size; i++) {
            JSONObject fbPost = (JSONObject) parsedPosts.get(i);
            if (!fbPost.containsKey("message")) {
                continue;
            }
            FacebookPost facebookPost = new FacebookPost.Builder(fbPost)
                    .User()
                    .Message()
                    .CreationDate()
                    .LikesCount()
                    .Id()
                    .Link()
                    .Build();
            _postList.add(facebookPost);
        }
        return GetOnlyPostsThatContainHashtag(hashtag);
    }

    private Collection<FacebookPost> GetOnlyPostsThatContainHashtag(String hashtag) throws IOException{
        Collection<String> hashtagList = new ArrayList<String>();
        hashtagList.add(hashtag);
        FacebookHashtagFilterer filterer = new FacebookHashtagFilterer(hashtagList);
        _postList = filterer.Filter(_postList);
        //FillPostlistWithComments();
        return _postList;
    }

    private void FillPostlistWithComments() throws IOException {
        FacebookCommentRetriever replies = new FacebookCommentRetriever(_connector, _parser);
        for (FacebookPost fbPost : _postList) {
            JSONArray parsedComments = replies.GetCommentsOfPost(fbPost.GetId());
            for (int j = 0; j < parsedComments.size(); j++) {
                JSONObject comment = (JSONObject) parsedComments.get(j);
                FacebookComment fbcomment = new FacebookComment.Builder(comment)
                        .User()
                        .Id()
                        .CreationDate()
                        .LikeCount()
                        .Message()
                        .Build();
                fbPost.AddComment(fbcomment);
            }
        }
    }

    private JSONArray RetrieveResults(String query) throws IOException {
        String feed = _connector.SearchQuery(query, _parser.GetTimestampLimit());
        JSONObject data = (JSONObject) JSONValue.parse(feed);
        return GetSearchResultUntilTimestamp(data, _searchResult);
    }

    private JSONArray GetSearchResultUntilTimestamp(JSONObject searchData, JSONArray result) throws IOException {
        JSONArray statuses = FacebookJsonParser.GetDataObject(searchData);
        String nextUrl = _parser.GetNextUrl(searchData);
        if (_parser.IsLastStatusNewerThanTimestamp(statuses) && nextUrl != null ) {
            JSONObject data = _connector.GetNextPage(nextUrl);
            result = JsonHelper.Concatenate(result, statuses);
            return GetSearchResultUntilTimestamp(data, result);
        } else {
            result = JsonHelper.Concatenate(result, statuses);
        }
        return _parser.TrimFeedUntilTimestamp(result);
    }


}


