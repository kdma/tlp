import java.io.IOException;
import java.text.ParseException;
import java.util.Collection;

/**
 * Created with IntelliJ IDEA.
 * User: kdma
 * Date: 18/06/13
 * Time: 17.47
 * To change this template use File | Settings | File Templates.
 */
public class FacebookResultBuilder implements IConnector<FacebookPost> {

    @Override
    public Collection<FacebookPost> GetUserStatuses(String userId, Long timestamp) throws IOException{
        WallFeed _feed = new WallFeed(userId, timestamp);
        return _feed.GetPostsOfUser();
    }

    @Override
    public Collection<FacebookPost> GetStatusesByHashtag(String hashtag, Long timestamp) throws IOException {
        FacebookSearch _searcher = new FacebookSearch(timestamp);
        return  _searcher.ExecuteSearch(hashtag);
    }
}
