import org.json.simple.JSONArray;
import org.json.simple.JSONObject;

import java.text.ParseException;

/*
 * Created with IntelliJ IDEA.
 * User: kdma
 * Date: 11/07/13
 * Time: 17.07
 * To change this template use File | Settings | File Templates.
 */

public class TwitterJsonParser implements ITimestampActions {

    private Long _timestampLimit;

    public TwitterJsonParser(Long timestampLimit){
        _timestampLimit = timestampLimit;
    }

    public String GetNextUrl(JSONObject data) {
        JSONObject metadata = (JSONObject) data.get("search_metadata");
        return (String) metadata.get("next_results");
    }

    public static JSONArray GetStatusesArray(JSONObject searchData) {
        return (JSONArray) searchData.get("statuses");
    }

    @Override
    public int GetIndexOfLastStatusLessThanTimestamp(JSONArray data){
        for (int i = 0; i < data.size(); i++) {
            JSONObject post = (JSONObject) data.get(i);
            try {
                Long postTimestamp = DateHelper.ConvertTwitterDateToTimestamp((String) post.get("created_at"));
                if (this._timestampLimit > postTimestamp) {
                    return i - 1;
                }
            }catch (ParseException e){
                System.out.println(e.getMessage());
            }
        }
        return data.size();
    }

    @Override
    public Long GetTimestampOfLastStatus(JSONArray statuses) {
        int last = statuses.size() - 1;
        JSONObject tweet = (JSONObject) statuses.get(last);
       try {
           return DateHelper.ConvertTwitterDateToTimestamp((String) tweet.get("created_at"));
       }          catch (ParseException e){
           System.out.println(e.getMessage());
       }
        return null;
    }

    @Override
    public boolean IsLastStatusNewerThanTimestamp(JSONArray data) {
        Long PostTimeStamp = GetTimestampOfLastStatus(data);
        return this._timestampLimit <= PostTimeStamp;
    }

    @Override
    public JSONArray TrimFeedUntilTimestamp(JSONArray statuses) {
        int limit = GetIndexOfLastStatusLessThanTimestamp(statuses);
        if (limit != statuses.size() || limit == -1) {
            statuses = JsonHelper.Resize(statuses, limit);
        }
        return statuses;
    }

    public Long GetTimestampLimit(){
        return _timestampLimit;
    }
}

