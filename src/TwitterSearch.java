import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.JSONValue;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;

/**
 * Created with IntelliJ IDEA.
 * User: kdma
 * Date: 19/07/13
 * Time: 18.13
 * To change this template use File | Settings | File Templates.
 */
public class TwitterSearch {

    private TwitterApiConnector _connector;
    private JSONArray _searchResult;
    private TwitterJsonParser _parser;
    private Collection<Tweet> _postList;

    public TwitterSearch(Long timestampLimit){
        _connector = new TwitterApiConnector();
        _searchResult = new JSONArray();
        _parser = new TwitterJsonParser(timestampLimit);
        _postList = new ArrayList<Tweet>();
    }

    public TwitterSearch(Long timestampLimit, Collection<Tweet> postlist){
        this(timestampLimit);
        _postList = postlist;
    }

        public Collection<Tweet> ExecuteSearch(String query) throws IOException {
        JSONArray parsedPosts = RetrieveResults(query);
        for (int i = 0, size = parsedPosts.size(); i < size; i++) {
            JSONObject parsedTweet = (JSONObject) parsedPosts.get(i);
            Tweet tweet = new Tweet.Builder(parsedTweet)
                    .Id()
                    .CreationDate()
                    .Timestamp()
                    .FavoriteCount()
                    .Hashtags()
                    .RetweetCount()
                    .Source()
                    .Text()
                    .Urls()
                    .User()
                    .UserMentions()
                    .Build();
            _postList.add(tweet);
        }
        //FillTweetListWithReplies(_parser.GetTimestampLimit());
        return _postList;
    }

    public void FillTweetListWithReplies(Long timestampLimit) throws IOException{
        ReplyRetriever _retriever = new ReplyRetriever(_postList, timestampLimit);
        _retriever.FillTweetListWithReplies();
    }

    public void FillTimelineWithReplies(Long timestampLimit, String accountName) throws IOException{
        ReplyRetriever _retriever = new ReplyRetriever(_postList, timestampLimit);
        _retriever.FillTimelineWithReplies(accountName);
    }

    private JSONArray GetReplies(String user) throws IOException {
        _searchResult = RetrieveResults(user);
        return _searchResult;
    }

    private JSONArray RetrieveResults(String query) throws IOException {
        String feed = _connector.SearchQuery(query);
        JSONObject data = (JSONObject) JSONValue.parse(feed);
        return GetSearchResultUntilTimestamp(data, _searchResult);
    }

    private JSONArray GetSearchResultUntilTimestamp(JSONObject searchData, JSONArray result) throws IOException {
        JSONArray statuses = TwitterJsonParser.GetStatusesArray(searchData);
        String nextUrl = _parser.GetNextUrl(searchData);
        if (_parser.IsLastStatusNewerThanTimestamp(statuses) && nextUrl != null ) {  //use timestamp of last tweet in feed or use timestampLimit set by caller?
            String searchFeed = _connector.GetNextPageOfSearch(nextUrl);
            JSONObject data = (JSONObject) JSONValue.parse(searchFeed);
            result = JsonHelper.Concatenate(result, statuses);
            return GetSearchResultUntilTimestamp(data, result);
        } else {
            result = JsonHelper.Concatenate(result, statuses);
        }
        return _parser.TrimFeedUntilTimestamp(result);
    }

    private static class ReplyRetriever {
        private Collection<Tweet> _postList;
        private Long _timestampLimit;
        private JSONArray _searchResult;

        private ReplyRetriever(Collection<Tweet> postList, Long timestamp){
            _postList = postList;
            _timestampLimit = timestamp;
        }

        private void FillTweetListWithReplies() throws IOException {
            TwitterSearch searcher = new TwitterSearch(_timestampLimit);
            for (Tweet tweet : _postList) {
                _searchResult  = searcher.GetReplies(tweet.GetAccountName());
                Collection<Tweet> replies = FindTweetReplies(tweet);
                for (Tweet reply : replies) {
                    tweet.AddReply(reply);
                }
            }
        }

        private void FillTimelineWithReplies(String accountName) throws IOException {
            TwitterSearch searcher = new TwitterSearch(_timestampLimit);
            _searchResult  = searcher.GetReplies(accountName);
            for (Tweet tweet : _postList) {
                Collection<Tweet> replies = FindTweetReplies(tweet);
                for (Tweet reply : replies) {
                    tweet.AddReply(reply);
                }
            }
        }

        private void AddReplyIfIdMatches(Tweet tweet, JSONObject tweetFromSearch, String postId) {
            String in_reply_to_status_id_str = (String) tweetFromSearch.get("in_reply_to_status_id_str");
            if (in_reply_to_status_id_str != null && in_reply_to_status_id_str.equalsIgnoreCase(postId)) {
                Tweet reply = new Tweet.Builder(tweetFromSearch)
                        .Id()
                        .CreationDate()
                        .Timestamp()
                        .Text()
                        .User()
                        .Build();
                tweet.AddReply(reply);
            }
        }

        private Collection<Tweet> FindTweetReplies(Tweet tweet) {
            JSONArray replies = new JSONArray();
            for (int i = 0, size = _searchResult.size(); i < size; i++) {
                JSONObject tweetFromSearch = (JSONObject) _searchResult.get(i);
                AddReplyIfIdMatches(tweet, tweetFromSearch, tweet.GetId());
            }
            return replies;
        }

    }

}
