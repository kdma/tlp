import java.sql.Timestamp;
import java.util.Collection;

/**
 * Created with IntelliJ IDEA.
 * User: kdma
 * Date: 19/08/13
 * Time: 20.16
 * To change this template use File | Settings | File Templates.
 */
public interface ISocial {

     String GetDirectLinkToStatus();
     Timestamp GetCreationDate();
     String GetStatusText();
     String ExtractPolarity();
     boolean ContainsHashtag(Collection<String> hashtagList);
}
