/**
 * Created with IntelliJ IDEA.
 * User: kdma
 * Date: 06/06/13
 * Time: 23.00
 * To change this template use File | Settings | File Templates.
 */
public class TwitterUser {

    private String _id_str;
    private String _name;
    private String _screen_name;

    public void SetUserId(Object id_str) {
        _id_str = (String) id_str;
    }

    public void SetName(Object name) {
        _name = (String) name;
    }

    public void SetScreenName(Object screen_name) {
        _screen_name = (String) screen_name;
    }

    public String GetAccountName() {
        return _screen_name;
    }
}
