import org.json.simple.JSONObject;

/**
 * Created with IntelliJ IDEA.
 * User: kdma
 * Date: 17/06/13
 * Time: 0.10
 * To change this template use File | Settings | File Templates.
 */
public class FacebookUser {
    private String _id;
    private String _name;

    public FacebookUser() {
    }

    public String GetId() {
        return _id;
    }

    public String GetName() {
        return _name;
    }

    private FacebookUser(Builder builder) {
        _id = builder._id;
        _name = builder._username;
    }

    public static class Builder {
        private String _id;
        private String _username;

        static class JSON {
            private final static String USER_ID = "id";
            private final static String USER_NAME = "name";
        }

        public Builder(JSONObject jsonUser) {
            _id = (String) (jsonUser.get(JSON.USER_ID));
            _username = (String) (jsonUser.get(JSON.USER_NAME));
        }

        public FacebookUser Build() {
            return new FacebookUser(this);
        }


    }
}
