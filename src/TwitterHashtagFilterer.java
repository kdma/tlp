import java.util.ArrayList;
import java.util.Collection;

/**
 * Created with IntelliJ IDEA.
 * User: kdma
 * Date: 13/06/13
 * Time: 18.35
 * To change this template use File | Settings | File Templates.
 */
public class TwitterHashtagFilterer implements IFilter<Tweet> {

    private Collection<String> _hashtags;

    public TwitterHashtagFilterer(Collection<String> hashtags) {
        _hashtags = hashtags;
    }

    public Collection<Tweet> Filter(Collection<Tweet> input) {
        Collection<Tweet> filtered = new ArrayList<Tweet>();
        for (Tweet singleTweet : input) {
            FilterTweets(singleTweet, filtered);
        }
        return filtered;
    }

    private void FilterTweets(Tweet singleTweet, Collection<Tweet> filtered) {
        if (singleTweet.ContainsHashtag(_hashtags)) {
            filtered.add(singleTweet);
        }
    }


}
