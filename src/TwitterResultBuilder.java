import java.io.IOException;
import java.text.ParseException;
import java.util.Collection;

/**
 * Created with IntelliJ IDEA.
 * User: kdma
 * Date: 20/07/13
 * Time: 17.12
 * To change this template use File | Settings | File Templates.
 */
public class TwitterResultBuilder implements IConnector<Tweet>{

    @Override
    public Collection<Tweet> GetUserStatuses(String userId,  Long timestamp) throws IOException {
        Timeline _timeline = new Timeline(userId, timestamp);
        return  _timeline.BuildTimeline();
    }

    @Override
    public Collection<Tweet> GetStatusesByHashtag(String hashtag,  Long timestamp) throws IOException {
        TwitterSearch _searcher = new TwitterSearch(timestamp);
        return  _searcher.ExecuteSearch(hashtag);
    }
}
