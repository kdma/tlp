/**
 * Created with IntelliJ IDEA.
 * User: kdma
 * Date: 30/08/13
 * Time: 18.54
 * To change this template use File | Settings | File Templates.
 */
public class DatabaseHelper {

    public static class QUERY{
        public final static String SEL_FB_PAGES = "SELECT Pagina,Id,Nome FROM sorgenti WHERE Tipo = 'facebook' ";
        public final static String SEL_TWITTER_PAGES = "SELECT Pagina,Id,Nome  FROM sorgenti WHERE Tipo = 'twitter' ";
        public final static String SEL_HASHTAG = "SELECT Pagina,Id,Nome  FROM sorgenti WHERE Tipo = 'hashtag' ";
        public final static String INSERT_STATUS = "INSERT IGNORE INTO post(ID,Link,Data,Polarity,ID_Fonte,Text) VALUES(?,?,?,?,?,?)";

        public final static String CREATE_STATUS_TABLE = "CREATE TABLE IF NOT EXISTS `post` (\n" +
                "    `ID` int(11) NOT NULL AUTO_INCREMENT,\n" +
                "    `Link` varchar(500) DEFAULT NULL,\n" +
                "    `Data` datetime DEFAULT NULL,\n" +
                "    `Polarity` enum('-1', '+1', '0') DEFAULT NULL,\n" +
                "    `ID_Fonte` int(11) DEFAULT NULL,\n" +
                "    `Text` varchar(20000) DEFAULT NULL,\n" +
                "    PRIMARY KEY (`ID`),\n" +
                "    UNIQUE KEY `Link_UNIQUE` (`Link`),\n" +
                "    KEY `ID_Fonte_idx` (`ID_Fonte`),\n" +
                "    CONSTRAINT `ID_Fonte` FOREIGN KEY (`ID_Fonte`)\n" +
                "        REFERENCES `sorgenti` (`ID`)\n" +
                "        ON DELETE NO ACTION ON UPDATE NO ACTION\n" +
                ")  ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=latin1";

        public final static String CREATE_SOURCE_TABLE ="CREATE TABLE IF NOT EXISTS `sorgenti` (\n" +
                        "  `ID` int(11) NOT NULL,\n" +
                        "  `Nome` varchar(30) DEFAULT NULL,\n" +
                        "  `Pagina` varchar(50) DEFAULT NULL,\n" +
                        "  `Link` varchar(500) DEFAULT NULL,\n" +
                        "  `Tipo` varchar(30) DEFAULT NULL,\n" +
                        "  `Autore` int(11) DEFAULT NULL,\n" +
                        "  `Icona` varchar(50) DEFAULT NULL,\n" +
                        "  PRIMARY KEY (`ID`)\n" +
                        ") ENGINE=InnoDB DEFAULT CHARSET=latin1\n";

        public final static String CREATE_EXAMPLE_ROWS = "insert ignore into sorgenti values\n" +
                "('0', 'Chiamarsi \\\"Bomber\\\" tra amici s', '118521558214401', NULL, 'facebook', NULL, NULL),\n" +
                " ('2', 'Wired Italia', 'wireditalia', NULL, 'twitter', NULL, NULL),\n" +
                "( '3', 'EA SPORTS FIFA ITA', 'EA_FIFA_Italia', NULL, 'twitter', NULL, NULL),\n" +
                " ('4', '#juventus', '#juventus', NULL, 'hashtag', NULL, NULL),\n" +
                " ('5', '#wikileaks', '#wikileaks', NULL, 'hashtag', NULL, NULL)";
    }



    public static class Structure {
        public final static String NAME = "Nome";
        public final static String ID = "Id";
        public final static String PAGE = "Pagina";
    }
}
