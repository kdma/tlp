import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;
import org.json.simple.JSONObject;
import org.json.simple.JSONValue;

import java.io.IOException;
import java.io.InputStream;
import java.net.URLEncoder;
import java.util.Scanner;

/**
 * Created with IntelliJ IDEA.
 * User: kdma
 * Date: 14/06/13
 * Time: 16.01
 * To change this template use File | Settings | File Templates.
 */
public class FacebookApiConnector {
    private HttpClient _client;
    private final static String ACCESS_TOKEN = "&access_token=CAACP2zbcg28BAM5lfE361HsOik74PYdV8JkchtKhyV9ZCOZBSL7ZApoRbcULmOPD5bt5XHMbwKaMVATLZBXbaCTK2jXdmbI5qHCWJPh6hpr0oH0tRWjU6JEan0VfXJrtqBnIoFboiwdOE32ZCg8Ct7oHnHRSF3WnvrtuYlMIa8AoCarn97APDJhwK3qPlgm8ZD";
    private final static String GRAPH_REQUEST = "https://graph.facebook.com/";
    private final static String TIMELINE_TEMPLATE = "feed?";
    private final static String FIELDS = "fields=id,created_time,message,with_tags,link,likes,from&date_format=U&since=";
    private final static String COMMENTS_TEMPLATE = "/comments?date_format=U";
    private final static String SEARCH_TEMPLATE = "search?q=";
    private final static String SEARCH_TYPE = "&type=post";
    public static final boolean DEV_MODE = false;

    public FacebookApiConnector() {
        _client = new DefaultHttpClient();
    }

    public String GetUserWallFeed(String page, Long timestamp) throws IOException {
        String request = GRAPH_REQUEST + page + TIMELINE_TEMPLATE + FIELDS + timestamp + ACCESS_TOKEN;
        return ExecuteRequest(request);
    }

    public String GetComments(String postid) throws IOException {
        return ExecuteRequest(GRAPH_REQUEST + postid + COMMENTS_TEMPLATE + ACCESS_TOKEN);
    }

    public String SearchQuery(String hashtag, Long timestampLimit) throws IOException {
        String encodedQuery = URLEncoder.encode(hashtag, "ISO-8859-1");
        return ExecuteRequest(GRAPH_REQUEST + SEARCH_TEMPLATE + encodedQuery + SEARCH_TYPE + "&" + FIELDS + timestampLimit + ACCESS_TOKEN);
    }

    public JSONObject GetNextPage(String nextUrl) throws IOException {
        String rawFeed = "{\"data\":[]}";
        if(nextUrl != null){
            rawFeed = ExecuteRequest(nextUrl);
        }
        return (JSONObject) JSONValue.parse(rawFeed);
    }

    public String ExecuteRequest(String request) throws IOException {
        HttpGet getRequest = new HttpGet(request);
        HttpResponse response = GetClient().execute(getRequest);
        System.out.println("executing request: " + getRequest.getRequestLine());
        return GetContent(response);
    }//TODO manage errors

    public String GetContent(HttpResponse response) throws IOException {
        if(DEV_MODE){
            int statusCode = response.getStatusLine().getStatusCode();
            System.out.println(statusCode + ":" + response.getStatusLine().getReasonPhrase());
            System.out.println("size of content " + response.getEntity().getContentLength());
            long start = System.currentTimeMillis();
            InputStream stream = response.getEntity().getContent();
            String feed = new Scanner(stream).useDelimiter("\\A").next();
            long elapsed = System.currentTimeMillis() - start;
            System.out.println("GetContent took " + elapsed + " ms");
            return feed;
        }else{
            int statusCode = response.getStatusLine().getStatusCode();
            System.out.println(statusCode + ":" + response.getStatusLine().getReasonPhrase());
            InputStream stream = response.getEntity().getContent();
            String feed = new Scanner(stream).useDelimiter("\\A").next();
            return feed;
        }
    }

    private HttpClient GetClient() {
        return _client;
    }

}
