import org.json.simple.JSONArray;
import org.json.simple.JSONObject;


/**
 * Created with IntelliJ IDEA.
 * User: kdma
 * Date: 04/07/13
 * Time: 12.07
 * To change this template use File | Settings | File Templates.
 */
public class FacebookJsonParser implements ITimestampActions {

    private Long _timestampLimit;

    public FacebookJsonParser(Long timestamp) {
        _timestampLimit = timestamp;
    }

    public  String GetNextUrl(JSONObject data) {
        JSONObject paging = (JSONObject) data.get("paging");
        if (paging.containsKey("next")) {
            return (String) paging.get("next");
        }
        return null;
    }

   public static JSONArray GetDataObject(JSONObject parsedFeed) {
        return (JSONArray) parsedFeed.get("data");
    }

    @Override
    public int GetIndexOfLastStatusLessThanTimestamp(JSONArray data) {
        for (int i = 0; i < data.size(); i++) {
            JSONObject post = (JSONObject) data.get(i);
            if (this._timestampLimit > GetCreationDateAsTimestamp(post)) {
                return i - 1;
            }
        }
        return data.size();
    }

    private Long GetCreationDateAsTimestamp(JSONObject post) {
        Long timestamp = (Long) post.get("created_time");
        //return DateHelper.ConvertFacebookTimestampToItalianTime(timestamp);
        return timestamp;
    }

    @Override
    public Long GetTimestampOfLastStatus(JSONArray statuses) {
        int last = statuses.size() - 1;
        JSONObject lastPost = (JSONObject) statuses.get(last);
        return GetCreationDateAsTimestamp(lastPost);
    }

    @Override
    public boolean IsLastStatusNewerThanTimestamp(JSONArray data) {
        Long PostTimeStamp = GetTimestampOfLastStatus(data);
        return this._timestampLimit <= PostTimeStamp;
    }

    @Override
    public JSONArray TrimFeedUntilTimestamp(JSONArray statuses) {
        int limit = GetIndexOfLastStatusLessThanTimestamp(statuses);
        if (limit != statuses.size()) {
            statuses = JsonHelper.Resize(statuses, limit);
        }
        return statuses;
    }

    public Long GetTimestampLimit(){
        return _timestampLimit;
    }
}
