import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.JSONValue;

import java.io.IOException;

/**
 * Created with IntelliJ IDEA.
 * User: kdma
 * Date: 26/07/13
 * Time: 15.13
 * To change this template use File | Settings | File Templates.
 */
public class FacebookCommentRetriever {

    private JSONArray _commentData;
    private FacebookApiConnector _connector;
    private FacebookJsonParser _parser;

    public FacebookCommentRetriever(FacebookApiConnector connector, FacebookJsonParser parser)
    {
        _connector = connector;
        _parser = parser;
        _commentData = new JSONArray();
    }

    public JSONArray GetCommentsOfPost(String postId) throws IOException{
        JSONArray comments =  new JSONArray();
        _commentData = GetCommentsWithPaging(GetRawComments(postId), comments);
        return _commentData;
    }

    private JSONArray GetCommentsWithPaging(JSONObject feed, JSONArray commentList) throws IOException {
        JSONArray comments = FacebookJsonParser.GetDataObject(feed);
        if (!comments.isEmpty()) {
            JSONObject page = _connector.GetNextPage(_parser.GetNextUrl(feed));
            commentList =  GetCommentsWithPaging(page, comments);
        }
        commentList = JsonHelper.Concatenate(comments, commentList);
        return commentList;
    }

    private JSONObject GetRawComments(String id) throws IOException {
        String feed = _connector.GetComments(id);
        return (JSONObject) JSONValue.parse(feed);
    }
}

