import oauth.signpost.OAuthConsumer;
import oauth.signpost.commonshttp.CommonsHttpOAuthConsumer;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;

import java.io.IOException;
import java.io.InputStream;
import java.net.URLEncoder;
import java.util.Scanner;


public class TwitterApiConnector {

    private HttpClient _client;
    private OAuthConsumer _consumer;

    private static final String AccessToken = "312222102-j10URUgvdg9RtJ8SgfuW1b2SwdYTj6YNeeIfrv4E";
    private static final String AccessSecret = "qfOLMKe1ita9oM3RvfW82NynbXej47vgHm6ieHuFc";
    private static final String ConsumerKey = "t6auWj5TFuKc1NNhg5ZbQ";
    private static final String ConsumerSecret = "P1Ziav2gyD3lXavk8sv3YJf507hYvdwmNJEHEb0X5Oc";
    private static final String TIMELINE_REQUEST = "https://api.twitter.com/1.1/statuses/user_timeline.json?screen_name=";
    private static final String SEARCH_REQUEST = "https://api.twitter.com/1.1/search/tweets.json";
    private static final String REQUEST_LIMIT = "&count=50";
    private static final String EXCLUDE_REPLIES = "&exclude_replies=true";
    public static final boolean DEV_MODE = false;

    public TwitterApiConnector() {
        _client = new DefaultHttpClient();
        _consumer = new CommonsHttpOAuthConsumer(ConsumerKey, ConsumerSecret);
        _consumer.setTokenWithSecret(AccessToken, AccessSecret);
    }
    //TODO manage errors and request limits
    public String GetUserTimeline(String user) throws IOException {
        return ExecuteRequest(TIMELINE_REQUEST + user + EXCLUDE_REPLIES + REQUEST_LIMIT);
    }

    public String GetUserTimelineAfterTweetId(String user, String tweetId) throws IOException {
        return ExecuteRequest(TIMELINE_REQUEST + user + "&max_id=" + tweetId + EXCLUDE_REPLIES +REQUEST_LIMIT);
    }

    public String SearchQuery(String query) throws IOException {
        String encodedQuery = URLEncoder.encode(query, "ISO-8859-1");
        return ExecuteRequest(SEARCH_REQUEST + "?&q=" + encodedQuery + REQUEST_LIMIT);
    }

    public String GetNextPageOfSearch(String query) throws IOException {
        return ExecuteRequest(SEARCH_REQUEST + query);
    }

    public String ExecuteRequest(String request) throws IOException {
        HttpGet getRequest = new HttpGet(request);
        try {
            _consumer.sign(getRequest);
        } catch (Exception e){
            e.getStackTrace(); //error signing request
        }
        HttpResponse response = GetClient().execute(getRequest);
        System.out.println("executing request: " + getRequest.getRequestLine());
        return GetContent(response);
    }

    public String GetContent(HttpResponse response) throws IOException {
        if(DEV_MODE){
            int statusCode = response.getStatusLine().getStatusCode();
            System.out.println(statusCode + ":" + response.getStatusLine().getReasonPhrase());
            System.out.println("size of content " + response.getEntity().getContentLength());
            long start = System.currentTimeMillis();
            InputStream stream = response.getEntity().getContent();
            String feed = new Scanner(stream).useDelimiter("\\A").next();
            long elapsed = System.currentTimeMillis() - start;
            System.out.println("GetContent took " + elapsed + " ms");
            return feed;
        }else{
            int statusCode = response.getStatusLine().getStatusCode();
            System.out.println(statusCode + ":" + response.getStatusLine().getReasonPhrase());
            InputStream stream = response.getEntity().getContent();
            String feed = new Scanner(stream).useDelimiter("\\A").next();
            return feed;
        }
    }

    public void CloseConnection() {
        GetClient().getConnectionManager().shutdown();
    }

    private HttpClient GetClient() {
        return _client;
    }
}