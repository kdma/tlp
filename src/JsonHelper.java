import org.json.simple.JSONArray;

/**
 * Created with IntelliJ IDEA.
 * User: kdma
 * Date: 12/07/13
 * Time: 15.26
 * To change this template use File | Settings | File Templates.
 */
public class JsonHelper {

    public static JSONArray Resize(JSONArray data, int size) {
        JSONArray result = new JSONArray();
        for (int i = 0; i <= size; i++) {
            result.add(data.get(i));
        }
        return result;
    }

    public static JSONArray Concatenate(JSONArray arr1, JSONArray arr2) {
        JSONArray result = new JSONArray();
        for (int i = 0; i < arr1.size(); i++) {
            result.add(arr1.get(i));
        }
        for (int i = 0; i < arr2.size(); i++) {
            result.add(arr2.get(i));
        }
        return result;
    }

}
