import java.util.ArrayList;
import java.util.Collection;

/**
 * Created with IntelliJ IDEA.
 * User: kdma
 * Date: 28/07/13
 * Time: 20.50
 * To change this template use File | Settings | File Templates.
 */
public class FacebookHashtagFilterer implements IFilter<FacebookPost>{

    private Collection<String> _hashtags;
    Collection<FacebookPost> _filtered;

    public FacebookHashtagFilterer(Collection<String> hashtags) {
        _hashtags = hashtags;
        _filtered  = new ArrayList<FacebookPost>();
    }

    public Collection<FacebookPost> Filter(Collection<FacebookPost> input) {
        for (FacebookPost post : input) {
            FilterMessage(post);
        }
        return _filtered;
    }

    private void FilterMessage(FacebookPost post) {
        if (post.ContainsHashtag(_hashtags)) {
            _filtered.add(post);
        }
    }

}
