import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.IOException;
import java.io.OutputStream;
import java.io.PrintStream;
import java.net.URL;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;

/**
 * Created with IntelliJ IDEA.
 * User: kdma
 * Date: 28/08/13
 * Time: 16.29
 * To change this template use File | Settings | File Templates.
 */
public class Gui {
    private JPanel panel1;
    private JComboBox _monthCb;
    private JComboBox _dayCb;
    private JComboBox _intervalCb;
    private JButton fetchStatusesButton;
    private JTextArea textArea;
    private JLabel updatedAt;
    private JButton stopButton;
    private JLabel runningStatus;

    private GuiController _controller;
    private int _month;
    private int _day;
    private int _interval;
    private Map<String, Integer> _intervalMap = new HashMap<String, Integer>();
    private ImageIcon _greenLightIcon;
    private ImageIcon _redLightIcon;

    public Gui() {
        _controller = new GuiController();
        LoadImages();
        SetupHashMap();
        SetDefaultValues();

        fetchStatusesButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent actionEvent) {
                runningStatus.setIcon(_greenLightIcon);
                fetchStatusesButton.setEnabled(false);
                redirectSystemStreams();
                _controller.runDataAggregator(_month, _day, _interval, updatedAt);
                stopButton.setEnabled(true);
            }
        });

        _monthCb.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent actionEvent) {
                JComboBox meseCb = (JComboBox) actionEvent.getSource();
                String mese = (String) meseCb.getSelectedItem();
                try {
                    Date date = new SimpleDateFormat("MMM", Locale.ITALIAN).parse(mese);
                    Calendar cal = Calendar.getInstance();
                    cal.setTime(date);
                    _month = cal.get(Calendar.MONTH);
                } catch (ParseException e) {
                    e.getStackTrace();
                }
            }
        });

        _dayCb.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent actionEvent) {
                JComboBox giornoCb = (JComboBox) actionEvent.getSource();
                _day = Integer.valueOf((String) giornoCb.getSelectedItem());
            }
        });

        _intervalCb.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent actionEvent) {
                JComboBox updateInterval = (JComboBox) actionEvent.getSource();
                String intervalValue = (String) updateInterval.getSelectedItem();
                _interval = _intervalMap.get(intervalValue);
            }
        });

        stopButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent actionEvent) {
                _controller.Stop();
                runningStatus.setIcon(_redLightIcon);
                stopButton.setEnabled(false);
                fetchStatusesButton.setEnabled(true);
            }
        });
    }

    private void updateTextArea(final String text) {
        SwingUtilities.invokeLater(new Runnable() {
            public void run() {
                textArea.append(text);
            }
        });
    }

    private void redirectSystemStreams() {
        OutputStream out = new OutputStream() {
            @Override
            public void write(int b) throws IOException {
                updateTextArea(String.valueOf((char) b));
            }

            @Override
            public void write(byte[] b, int off, int len) throws IOException {
                updateTextArea(new String(b, off, len));
            }

            @Override
            public void write(byte[] b) throws IOException {
                write(b, 0, b.length);
            }
        };

        System.setOut(new PrintStream(out, true));
        System.setErr(new PrintStream(out, true));
    }

    private void SetupHashMap() {
        _intervalMap.put("15 minuti", 15);
        _intervalMap.put("30 minuti", 30);
        _intervalMap.put("1 ora", 60);
        _intervalMap.put("3 ore", 180);
        _intervalMap.put("6 ore", 360);
        _intervalMap.put("12 ore", 720);
        _intervalMap.put("24 ore", 1440);
    }

    private void SetDefaultValues() {
        _interval = _intervalMap.get("15 minuti");
        Calendar today  = Calendar.getInstance(TimeZone.getTimeZone("Europe/Rome"));
        _month = today.get(Calendar.MONTH);
        _day = today.get(Calendar.DAY_OF_MONTH);
        _monthCb.setSelectedIndex(_month);
        _dayCb.setSelectedIndex(_day - 1);
        runningStatus.setIcon(_redLightIcon);
    }

    private void LoadImages(){
        _greenLightIcon = new ImageIcon(getClass().getResource("res/greenLight.gif"));
        _redLightIcon = new ImageIcon(getClass().getResource("res/redLight.gif"));
    }

    public static void main(String[] args) {
        JFrame frame = new JFrame("Data Aggregator TLP");
        URL url = ClassLoader.getSystemResource("res/icon.png");
        frame.setIconImage(Toolkit.getDefaultToolkit().getImage(url));
        frame.setContentPane(new Gui().panel1);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.pack();
        frame.setVisible(true);
    }

}
