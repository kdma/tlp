import java.io.IOException;
import java.sql.*;
import java.util.*;

/**
 * Created with IntelliJ IDEA.
 * User: kdma
 * Date: 31/07/13
 * Time: 17.18
 * To change this template use File | Settings | File Templates.
 */
public class DataAggregator {

    private Long _timestamp;
    Statement _fbStatement;
    Statement _twStatement;
    Statement _statementHashtags;

    private int _rowInserted = 0;

    public DataAggregator(Calendar italyCalendar) {
        _timestamp = italyCalendar.getTimeInMillis() / 1000;
    }

    public void InsertResults() throws SQLException, IOException {
        Connection connection = DatabaseConnection.GetConnection();
        FacebookResultBuilder fb = new FacebookResultBuilder();
        TwitterResultBuilder twitter = new TwitterResultBuilder();

        _fbStatement = connection.createStatement();
        ResultSet facebookSet = _fbStatement.executeQuery(DatabaseHelper.QUERY.SEL_FB_PAGES);
        _twStatement = connection.createStatement();
        ResultSet twitterSet = _twStatement.executeQuery(DatabaseHelper.QUERY.SEL_TWITTER_PAGES);
        FetchResultsForEachPage(fb, facebookSet, _timestamp);
        FetchResultsForEachPage(twitter, twitterSet, _timestamp);

        _statementHashtags = connection.createStatement();
        ResultSet rsHashtags = _statementHashtags.executeQuery(DatabaseHelper.QUERY.SEL_HASHTAG);

        ArrayList<IConnector> list = new ArrayList<IConnector>();
        list.add(fb);
        list.add(twitter);
        InsertStatusWithHashtagsToDB(list, rsHashtags, _timestamp);

    }

    private void FetchResultsForEachPage(IConnector resultBuilder, ResultSet rs, Long date) throws SQLException, IOException {
        if (!rs.isBeforeFirst() ) {
            System.out.println("There are no pages to search for in Db");
        }
        while (rs.next()) {
            String page = rs.getString(DatabaseHelper.Structure.PAGE);
            int fonteID = rs.getInt(DatabaseHelper.Structure.ID);
            String pageName = rs.getString(DatabaseHelper.Structure.NAME);
            System.out.println("Fetching results for " + pageName);
            InsertStatusesToDB(resultBuilder.GetUserStatuses(page, date), fonteID);
        }
        rs.close();
    }

    private void InsertStatusWithHashtagsToDB(Collection<IConnector> resultBuilders, ResultSet rsHashtags, Long date) throws SQLException, IOException {

            if (!rsHashtags.isBeforeFirst() ) {
                System.out.println("There are no hashtags to search for in Db");
            }else{
                    while (rsHashtags.next()) {
                    _rowInserted++;
                    String hashtag = rsHashtags.getString(DatabaseHelper.Structure.PAGE);
                    int id = rsHashtags.getInt(DatabaseHelper.Structure.ID);
                        for (IConnector SocialConnector : resultBuilders) {
                            System.out.println("Fetching results for " + hashtag );
                            InsertStatusesToDB(SocialConnector.GetStatusesByHashtag(hashtag, date), id);
                        }
                }
            }
        rsHashtags.close();
    }

    private <T extends ISocial> void InsertStatusesToDB(Collection<T> postlist, int fonteID) throws SQLException {
        Connection connection = DatabaseConnection.GetConnection();
        PreparedStatement statement = connection.prepareStatement(DatabaseHelper.QUERY.INSERT_STATUS);
        for (ISocial post : postlist) {
            statement.setInt(1, 0);
            statement.setString(2, post.GetDirectLinkToStatus());
            statement.setTimestamp(3, post.GetCreationDate());
            statement.setString(4, post.ExtractPolarity());
            statement.setInt(5, fonteID);
            statement.setString(6, post.GetStatusText());
             _rowInserted += statement.executeUpdate();
        }
        statement.close();
     }

    public int GetNumberOfRowsInserted(){
        return _rowInserted;
    }

    public void CloseResources() throws SQLException{
        _fbStatement.close();
        _twStatement.close();
        _statementHashtags.close();
    }

    public void ResetRowCount(){
        _rowInserted = 0;
    }

}


