import org.json.simple.JSONArray;
import org.json.simple.JSONObject;

/**
 * Created with IntelliJ IDEA.
 * User: kdma
 * Date: 20/07/13
 * Time: 12.20
 * To change this template use File | Settings | File Templates.
 */
public interface ITimestampActions {

    int GetIndexOfLastStatusLessThanTimestamp(JSONArray data);
    Long GetTimestampOfLastStatus(JSONArray statuses);
    boolean IsLastStatusNewerThanTimestamp(JSONArray data);
    JSONArray TrimFeedUntilTimestamp(JSONArray statuses);
     String GetNextUrl(JSONObject data);
}
