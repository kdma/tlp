import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;
import java.util.TimeZone;

/**
 * Created with IntelliJ IDEA.
 * User: kdma
 * Date: 17/07/13
 * Time: 23.24
 * To change this template use File | Settings | File Templates.
 */
public class DateHelper {

    public static Long ConvertTwitterDateToTimestamp(String date) throws ParseException {
        SimpleDateFormat dateFormat = new SimpleDateFormat("EEE MMM dd HH:mm:ss ZZZZZ yyyy", Locale.ENGLISH);
        dateFormat.setLenient(false);
        Date parsed = dateFormat.parse(date);
        Long timestamp = parsed.getTime();
        return timestamp / 1000;

    }
}
