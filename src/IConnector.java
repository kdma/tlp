import java.io.IOException;
import java.text.ParseException;
import java.util.Collection;

/**
 * Created with IntelliJ IDEA.
 * User: kdma
 * Date: 15/07/13
 * Time: 17.21
 * To change this template use File | Settings | File Templates.
 */
public interface IConnector<T> {
    Collection<T> GetUserStatuses(String userId, Long timestamp) throws  IOException;
    Collection<T> GetStatusesByHashtag(String hashtag, Long timestamp) throws  IOException;
}
