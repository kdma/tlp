import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.JSONValue;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;
/**
 * Created with IntelliJ IDEA.
 * User: kdma
 * Date: 18/07/13
 * Time: 0.32
 * To change this template use File | Settings | File Templates.
 */
public class WallFeed {
    private Long _timestampLimit;
    private String _user;
    private FacebookApiConnector _connector;
    private Collection<FacebookPost> _postlist;
    private FacebookJsonParser _parser;

    public WallFeed(String user , Long timestampLimit){
        _postlist = new ArrayList<FacebookPost>();
        _user = user;
        _timestampLimit = timestampLimit;
        _connector = new FacebookApiConnector();
        _parser = new FacebookJsonParser(_timestampLimit);
    }

    public Collection<FacebookPost> GetPostsOfUser() throws IOException {
        JSONArray parsedTimeline = GetPostsWithPaging(GetFirstPage());
        for (int i = 0; i < parsedTimeline.size(); i++) {
            JSONObject fbPost = (JSONObject) parsedTimeline.get(i);
            if (!fbPost.containsKey("message") && !fbPost.containsKey("picture")) {
                continue;
            }
            FacebookPost facebookPost = new FacebookPost.Builder(fbPost)
                    .User()
                    .Message()
                    .CreationDate()
                    .LikesCount()
                    .Id()
                    .Link()
                    .Build();
            _postlist.add(facebookPost);
        }
        FillPostlistWithComments();
        return _postlist;
    }

    private void FillPostlistWithComments() throws IOException  {
        FacebookCommentRetriever replies = new FacebookCommentRetriever(_connector,_parser);
        for (FacebookPost fbPost : _postlist) {
            JSONArray parsedComments = replies.GetCommentsOfPost(fbPost.GetId());
            for (int j = 0; j < parsedComments.size(); j++) {
                JSONObject comment = (JSONObject) parsedComments.get(j);
                FacebookComment fbcomment = new FacebookComment.Builder(comment)
                        .User()
                        .Id()
                        .CreationDate()
                        .LikeCount()
                        .Message()
                        .Build();
                fbPost.AddComment(fbcomment);
            }
        }
    }

    private JSONArray GetPostsWithPaging(String rawFeed) throws  IOException{
        JSONObject parsedFeed = (JSONObject) JSONValue.parse(rawFeed);
        JSONArray temp_data = FacebookJsonParser.GetDataObject(parsedFeed);
        return GetNextPageUntilTimestamp(temp_data, parsedFeed);
    }

    private JSONArray GetNextPageUntilTimestamp(JSONArray postList, JSONObject feed) throws IOException {
        JSONArray pagePosts = new JSONArray();
        if (!postList.isEmpty()) {
            if (_parser.IsLastStatusNewerThanTimestamp(postList)) {
                JSONObject parsedFeed = GetNextPage(feed);
                pagePosts = FacebookJsonParser.GetDataObject(parsedFeed);
                postList = JsonHelper.Concatenate(postList, pagePosts);
                return GetNextPageUntilTimestamp(postList, parsedFeed);
            } else {
                postList = _parser.TrimFeedUntilTimestamp(JsonHelper.Concatenate(postList, pagePosts));
            }
        }
        return postList;
    }

    public JSONObject GetNextPage(JSONObject postList) throws IOException {
        String next = _parser.GetNextUrl(postList);
        String rawFeed = "{\"data\":[]}";
        if(next != null){
            rawFeed = _connector.ExecuteRequest(next);
        }
        return (JSONObject) JSONValue.parse(rawFeed);
    }

    private String GetFirstPage() throws IOException {
        return _connector.GetUserWallFeed(_user + "/", GetTimestamp());
    }

    private Long GetTimestamp() {
        return _timestampLimit;
    }


}
