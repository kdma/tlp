import org.json.simple.JSONArray;
import org.json.simple.JSONObject;

import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.*;

/**
 * Created with IntelliJ IDEA.
 * User: kdma
 * Date: 06/06/13
 * Time: 22.49
 * To change this template use File | Settings | File Templates.
 */
public class Tweet implements ISocial{

    private TwitterUser _user;
    private String _id_str;
    private Timestamp _created_at;
    private Long _timestamp;
    private String _text;
    private String _source;
    private Long _retweet_count;
    private Long _favorite_count;
    private Collection<String> _hashtags;
    private Collection<String> _urls;
    private Collection<String> _user_mentions;
    private Collection<Tweet> _replies;

    /*
   [
     {
        "id_str": "345197766957486080",
            "created_at": "Thu Jun 13 15:15:35 +0000 2013",
            "source": "<a href=\"http: //www.tweetdeck.com\" rel=\"nofollow\">TweetDeck</a>",
            "text": "Tutto quello che c'è da sapere sul nuovo #myspace http://t.co/zvXYsOyoaN",
            "favorite_count": 1,
            "retweet_count": 2,
            "user": {
                    "id_str": "119711344",
                    "name": "Wired Italia",
                    "screen_name": "wireditalia"
                    },
        "entities": {
                    "hashtags": [
                                {
                                  "text": "myspace"
                                }
                    ],
                    "urls": [
                                {
                                  "url": "http://t.co/zvXYsOyoaN"
                                }
                    ],
                    "user_mentions": []
                    }
     }
   ]
    */

    public Tweet() {
        _urls = new ArrayList<String>();
        _hashtags = new ArrayList<String>();
        _user_mentions = new ArrayList<String>();
        _replies = new ArrayList<Tweet>();
        _user = new TwitterUser();
    }

    @Override
    public String GetDirectLinkToStatus(){
        return "https://twitter.com/"+_user.GetAccountName()+"/status/"+_id_str;
    }

    @Override
    public Timestamp GetCreationDate() {
        Timestamp createdTime = new Timestamp(_created_at.getTime());
        return createdTime;
    }

    @Override
    public String GetStatusText(){
        return _text;
    }

    @Override
    public String ExtractPolarity(){
        return "0";
    }

    @Override
    public boolean ContainsHashtag(Collection<String> hashtagList) {
        for (String singleHashtag : this.GetHashTags()) {
            for (String field : hashtagList) {
                if (singleHashtag.equalsIgnoreCase(field)) return true;
            }
        }
        return false;
    }

    public void AddReply(Tweet reply) {
        _replies.add(reply);
    }

    public Collection<Tweet> GetReplies() {
        return _replies;
    }

    public TwitterUser GetUser() {
        return _user;
    }

    public String GetAccountName() {
        return _user.GetAccountName();
    }

    public String GetId() {
        return _id_str;
    }

    public Long GetCreationDateAsTimestamp() {
        return _timestamp;
    }

    public String GetText() {
        return _text;
    }

    public String GetSource() {
        return _source;
    }

    public Long GetRetweetCount() {
        return _retweet_count;
    }

    public Long GetFavoriteCount() {
        return _favorite_count;
    }

    public Collection<String> GetHashTags() {
        return _hashtags;
    }

    public Collection<String> GetUrls() {
        return _urls;
    }

    public Collection<String> GetUserMentions() {
        return _user_mentions;
    }



    private Tweet(Builder builder) {
        this();
        _user = builder._user;
        _id_str = builder._id_str;
        _created_at = builder._created_at;
        _timestamp = builder._timestamp;
        _text = builder._text;
        _source = builder._source;
        _retweet_count = builder._retweet_count;
        _favorite_count = builder._favorite_count;
        _hashtags = builder._hashtags;
        _urls = builder._urls;
        _user_mentions = builder._user_mentions;
    }

    static class JSON {
        private static final String ID_FIELD = "id_str";
        private static final String DATE_FIELD = "created_at";
        private static final String SOURCE_FIELD = "source";
        private static final String TEXT_FIELD = "text";
        private static final String FAV_COUNT = "favorite_count";
        private static final String RT_COUNT = "retweet_count";
        private static final String USER_OBJ = "user";
        private static final String NAME_FIELD = "name";
        private static final String SCR_NAME = "screen_name";
        private static final String ENTITY_OBJ = "entities";
        private static final String HASHTAG_OBJ = "hashtags";
        private static final String URL_OBJ = "urls";
        private static final String URL_FIELD = "url";
        private static final String USER_MENTION_OBJ = "user_mentions";
        private static final String DATE_FORMAT = "EEE MMM dd HH:mm:ss ZZZZZ yyyy";
    }

    public static class Builder {

        private JSONObject _tweetJson;
        private JSONObject _entities;
        private TwitterUser _user;
        private String _id_str;
        private Timestamp _created_at;
        private Long _timestamp;
        private String _text;
        private String _source;
        private Long _retweet_count;
        private Long _favorite_count;
        private Collection<String> _hashtags;
        private Collection<String> _urls;
        private Collection<String> _user_mentions;

        public Builder(JSONObject tweet) {
            _tweetJson = tweet;
            _entities = (JSONObject) _tweetJson.get(JSON.ENTITY_OBJ);
        }

        public Builder User() {
            JSONObject user = (JSONObject) _tweetJson.get(JSON.USER_OBJ);
            _user = new TwitterUser();
            _user.SetName(user.get(JSON.NAME_FIELD));
            _user.SetUserId(user.get(JSON.ID_FIELD));
            _user.SetScreenName(user.get(JSON.SCR_NAME));
            return this;
        }

        public Builder Id() {
            _id_str = (String) _tweetJson.get(JSON.ID_FIELD);
            return this;
        }

        public Builder CreationDate() {
            String unformattedDate = (String) _tweetJson.get(JSON.DATE_FIELD);
            SimpleDateFormat sf = new SimpleDateFormat(JSON.DATE_FORMAT, Locale.ENGLISH);
            sf.setLenient(true);
            Date twitterDate = null;
            try {
                twitterDate = sf.parse(unformattedDate);
                _created_at = new Timestamp(twitterDate.getTime());
            } catch (Exception e) {
                e.getStackTrace();
            }
            return this;
        }

        public Builder Text() {
            _text = (String) _tweetJson.get(JSON.TEXT_FIELD);
            return this;
        }

        public Builder Source() {
            _source = (String) _tweetJson.get(JSON.SOURCE_FIELD);
            return this;
        }

        public Builder RetweetCount() {
            _retweet_count = (Long) _tweetJson.get(JSON.RT_COUNT);
            return this;
        }

        public Builder FavoriteCount() {
            _favorite_count = (Long) _tweetJson.get(JSON.FAV_COUNT);
            return this;
        }

        public Builder Hashtags() {
            _hashtags = new ArrayList<String>();
            JSONArray selected_entity = (JSONArray) _entities.get(JSON.HASHTAG_OBJ);
            for (int i = 0; i < selected_entity.size(); i++) {
                JSONObject singleHashtag = (JSONObject) selected_entity.get(i);
                String hashtag = (String) singleHashtag.get(JSON.TEXT_FIELD);
                _hashtags.add(hashtag);
            }
            return this;
        }

        public Builder Urls() {
            _urls = new ArrayList<String>();
            JSONArray selected_entity = (JSONArray) _entities.get(JSON.URL_OBJ);
            for (int i = 0; i < selected_entity.size(); i++) {
                JSONObject singleUrl = (JSONObject) selected_entity.get(i);
                String url = (String) singleUrl.get(JSON.URL_FIELD);
                _urls.add(url);
            }
            return this;
        }

        public Builder Timestamp() {
            String unformattedDate = (String) _tweetJson.get(JSON.DATE_FIELD);
            SimpleDateFormat sf = new SimpleDateFormat(JSON.DATE_FORMAT, Locale.ENGLISH);
            sf.setLenient(true);
            Long twitterDate = null;
            try {
                sf.setTimeZone(TimeZone.getTimeZone("UTC+1:00"));
                Date parsed = sf.parse(unformattedDate);
                twitterDate = parsed.getTime();
            } catch (Exception e) {
                System.out.println(e.getMessage());
            }
            _timestamp = twitterDate;
            return this;
        }


        public Builder UserMentions() {
            _user_mentions = new ArrayList<String>();
            JSONArray selected_entity = (JSONArray) _entities.get(JSON.USER_MENTION_OBJ);
            for (int i = 0; i < selected_entity.size(); i++) {
                JSONObject singleMention = (JSONObject) selected_entity.get(i);
                String mention = (String) singleMention.get(JSON.SCR_NAME);
                _user_mentions.add(mention);
            }
            return this;
        }

        public Tweet Build() {
            return new Tweet(this);
        }

    }

}

