import org.json.simple.JSONObject;

import java.sql.Timestamp;
import java.util.Date;


/**
 * Created with IntelliJ IDEA.
 * User: kdma
 * Date: 03/06/13
 * Time: 23.03
 * To change this template use File | Settings | File Templates.
 */

public class FacebookComment {

    private FacebookUser _user;
    private String _id;
    private String _message;
    private Long _like_count;
    private Date _created_time;

    private FacebookComment(Builder builder) {
        _id = builder._id;
        _message = builder._message;
        _like_count = builder._like_count;
        _created_time = builder._created_time;
        _user = builder._user;
    }

    public String GetId() {
        return _id;
    }

    public String GetMessage() {
        return _message;
    }

    public Long GetLikesCount() {
        return _like_count;
    }

    public static class Builder {

        private FacebookUser _user;
        private String _id;
        private String _message;
        private Long _like_count;
        private Timestamp _created_time;
        private JSONObject _comment;
        private JSONObject _userJson;

        static class JSON {
            private final static String FROM_OBJ = "from";
            private final static String COMMENT_ID = "id";
            private final static String MESSAGE = "message";
            private final static String LIKES = "like_count";
            private final static String CREATED_TIME = "created_time";
            private final static String DATE_FORMAT = "yyyy-MM-dd'T'HH:mm:ss+SSSS";
        }

        public Builder(JSONObject comment) {
            _comment = comment;
            _userJson = (JSONObject) comment.get(JSON.FROM_OBJ);
        }

        public Builder User() {
            _user = new FacebookUser.Builder(_userJson).Build();
            return this;
        }

        public Builder Id() {
            _id = (String) _comment.get(JSON.COMMENT_ID);
            return this;
        }

        public Builder Message() {
            _message = (String) _comment.get(JSON.MESSAGE);
            return this;
        }

        public Builder LikeCount() {
            _like_count = (Long) _comment.get(JSON.LIKES);
            return this;
        }

        public Builder CreationDate() {
            Long unformattedDate = (Long) _comment.get(JSON.CREATED_TIME);
            _created_time  = new Timestamp(unformattedDate * 1000);
            return this;
        }

        public FacebookComment Build() {
            return new FacebookComment(this);
        }

    }
}
