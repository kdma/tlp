import org.json.simple.JSONObject;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Collection;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class FacebookPost implements ISocial{

    private String _id;
    private Timestamp _created_time;
    private Long _likes_count;
    private String _link;
    private String _message;
    private Collection<String> _hashtags;
    private Collection<FacebookComment> _comments;
    private FacebookUser _user;
    private final static String HASTHTAG_REGEX = "(#[\\w-]+)";

    public FacebookPost() {
        _comments = new ArrayList<FacebookComment>();
        _user = new FacebookUser();
    }

    @Override
    public String GetDirectLinkToStatus(){
        return "http://www.facebook.com/" + _id ;
    }

    @Override
    public Timestamp GetCreationDate(){
        return _created_time;
    }

    @Override
    public String GetStatusText(){
        return _message;
    }

    @Override
    public String ExtractPolarity(){
        return "0";
    }

    @Override
    public boolean ContainsHashtag(Collection<String> hashtagList) {
        ParseHashTags();
        for (String singleHashtag : _hashtags) {
            for (String field : hashtagList) {
                if (singleHashtag.equalsIgnoreCase(field)) return true;
            }
        }
        return false;
    }


    public String GetId() {
        return _id;
    }

    public void AddComment(FacebookComment comment) {
        _comments.add(comment);
    }

    public String GetLink(){
        return _link;
    }

    public String GetAuthorName(){
        return _user.GetName();
    }


    public Collection<String> GetHashtags(){
        ParseHashTags();
        return _hashtags;
    }

    private void ParseHashTags(){
        Pattern pattern = Pattern.compile(HASTHTAG_REGEX);
        Matcher match = pattern.matcher(this._message);
        _hashtags = new ArrayList<String>();
        while (match.find()){
           _hashtags.add(match.group());
        }
    }

    private FacebookPost(Builder builder) {
        this();
        _user = builder._user;
        _message = builder._message;
        _created_time = builder._created_time;
        _id = builder._id;
        _link = builder._link;
        _likes_count = builder._likes_count;
    }

    public static class Builder {

        private FacebookUser _user;
        private String _id;
        private Timestamp _created_time;
        private Long _likes_count;
        private String _link;
        private String _message;
        private JSONObject _userJson;
        private JSONObject _postJson;
        private JSONObject _likesJson;

        static class JSON {
            private final static String POST_ID = "id";
            private final static String LIKE_OBJ = "likes";
            private final static String LIKES_COUNT = "count";
            private final static String LINK = "link";
            private final static String MESSAGE = "message";
            private final static String CREATED_TIME = "created_time";
            //private final static String DATE_FORMAT = "yyyy-MM-dd'T'HH:mm:ss+SSSS";  now returning a timestamp no need to parse
            private final static String FROM_OBJ = "from";
        }

        public Builder(JSONObject post) {
            _postJson = post;
            _userJson = (JSONObject) _postJson.get(JSON.FROM_OBJ);
            _likesJson = (JSONObject) _postJson.get(JSON.LIKE_OBJ);
        }

        public Builder User() {
            _user = new FacebookUser.Builder(_userJson).Build();
            return this;
        }

        public Builder Id() {
            _id = (String) _postJson.get(JSON.POST_ID);
            return this;
        }

        public Builder Message() {
            _message = (String) _postJson.get(JSON.MESSAGE);
            return this;
        }

        public Builder CreationDate() {
            Long unformattedDate = (Long) _postJson.get(JSON.CREATED_TIME);
            _created_time = new Timestamp(unformattedDate * 1000);
            return this;
        }

        public Builder LikesCount() {
            if (_likesJson != null) {
                _likes_count = (Long) _likesJson.get(JSON.LIKES_COUNT);
            }
            return this;
        }

        public Builder Link() {
            _link = (String) _postJson.get(JSON.LINK);
            return this;
        }

        public FacebookPost Build() {
            return new FacebookPost(this);
        }
    }
}
